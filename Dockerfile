FROM mcr.microsoft.com/windows/servercore/iis:windowsservercore-ltsc2022
SHELL ["cmd", "/S", "/C"]
COPY README.md C:\TEMP\

RUN powershell Install-WindowsFeature Web-Mgmt-Service;
